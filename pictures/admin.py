from django.contrib import admin

from .models import Picture, Image, General


admin.site.register(General)
admin.site.register(Picture)
admin.site.register(Image)
