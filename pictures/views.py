from django.shortcuts import render as _render

from .models import Picture, Image, General


def render(request, template_name, *args, **kwargs):
    return _render(request, template_name + ".html", *args, **kwargs)


def home(request):
    pictures = Picture.objects.all()
    general = General.objects.first()
    context = {
        'pictures': pictures,
        'general': general,
    }
    return render(request, 'home', context)


def picture(request, id):
    picture = Picture.objects.get(id=id)
    context = {
        'picture': picture
    }
    return render(request, 'picture', context)
