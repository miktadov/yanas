from django.db import models


class General(models.Model):
    image = models.ImageField(upload_to="pictures/general/images/")
    

class Picture(models.Model):
    title = models.CharField(max_length=64)
    price = models.PositiveIntegerField(default=0, blank=True)
    color = models.CharField(max_length=64, default="", blank=True)
    material = models.CharField(max_length=64, default="", blank=True)
    info = models.TextField(default="", blank=True)
    size = models.CharField(max_length=32, default="", blank=True)
    
    is_sold = models.BooleanField(default=False)
    is_publicated = models.BooleanField(default=True)

    def __str__(self) -> str:
        return self.title

    @property
    def image(self):
        try:
            image = self.image_set.get(is_general=True)
            return image.image.url
        except Image.DoesNotExist:
            return "not found"


class Image(models.Model):
    image = models.ImageField(upload_to="pictures/images/")
    picture = models.ForeignKey(Picture, on_delete=models.CASCADE)
    is_general = models.BooleanField(default=True)

    def __str__(self) -> str:
        return self.picture.title
